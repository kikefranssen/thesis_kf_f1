########### -------- Libraries -------- ###########
import numpy as np
import pandas as pd

########### -------- The functions that were made -------- ###########
# Pre-processing --> splitting variables and making them numeric
def splitting_and_numeric(column, denom):
    column = ''.join(column)
    answer, delete1, delete2 = column.split(denom) 
    answer = pd.to_numeric(answer, errors='coerce')
    return answer

# Pre-processing -->  turning the time variables into milliseconds
def to_milliseconds(column):
    if isinstance(column, float):
        fastest_time = 0
    elif ":" not in column:
        fastest_time = 0 
    else:
        minute, sec = column.split(':')
        sec, milisec = sec.split('.') 
        minute, sec, milisec = pd.to_numeric([minute,sec,milisec], errors='coerce')
        minute = minute*60*1000
        sec = sec*1000
        fastest_time = minute + sec + milisec
    return fastest_time

########### -------- Loading in data -------- ###########
df_beforePre = pd.read_csv(r'PUT PATH TO DF_BEFORE_PREPROCESSING.CSV FILE HERE')

########### -------- Dealing with Missing data -------- ###########
# Turning '\\N' values into NaN to get a true estimate of the amount of NA's in the dataset
df_beforePre = df_beforePre.replace('\\N', np.nan)

#Dealing with NA's --> Variables with NA are fastestLapTime, alt, q1,q2,q3, constructor point, constructor pos and stop. fastestLapTime, q1,q2,q3 will be dealt with later
df_beforePre.alt = df_beforePre.alt.fillna(7) # Online research showed an altitude of 7 for this circuit
df_beforePre.position = df_beforePre.position.fillna(0)

# Drop data about drivers who did not get to compete in the race, not relevant for the eventual prediction
df_beforePre = df_beforePre.dropna(subset=['constructor_point'], how='all') # drop data about drivers who did not get to compete in the race, not relevant for the eventual prediction

########### --------  Type Changing Operations  -------- ###########
#Changing the type of some values to make it fit into prediction, object variables are fastestLapTime, racetime, alt, q1,q2,q3, dob
df_beforePre.alt = pd.to_numeric(df_beforePre.alt)

#Change the date of birth variable to age by subtracting the 2022 by the year
df_beforePre['age'] = df_beforePre.apply(lambda row: splitting_and_numeric(row['dob'], "-"), axis=1)
df_beforePre.age = 2022 - df_beforePre.age

# Show only the hour of race_time to highlight the time of day the race is held, by splitting the race_time variable by the denominator
df_beforePre['start_time'] = df_beforePre.apply(lambda row: splitting_and_numeric(row['race_time'], ":"), axis=1)

# Splitting the q1 column to turn it into a numeric milliseconds variable
df_beforePre['q1_time'] = df_beforePre.apply(lambda row: to_milliseconds(row['q1']), axis=1)
df_beforePre.q1_time = df_beforePre.q1_time.fillna(0) # NA means that there was no lap time set, which translates into a 0 value.

# Splitting the q2 column to turn it into a numeric milliseconds variable
df_beforePre['q2_time'] = df_beforePre.apply(lambda row: to_milliseconds(row['q2']), axis=1)
df_beforePre.q1_time = df_beforePre.q1_time.fillna(0) # NA means that there was no lap time set, which translates into a 0 value.

# Splitting the q3 column to turn it into a numeric milliseconds variable
df_beforePre['q3_time'] = df_beforePre.apply(lambda row: to_milliseconds(row['q3']), axis=1)
df_beforePre.q1_time = df_beforePre.q1_time.fillna(0) # NA means that there was no lap time set, which translates into a 0 value.

#Dropping the unnecessary variables
df_beforePre = df_beforePre.drop(['q1', 'q2', 'q3', 'dob', 'race_time'], axis=1) 

#Save the dataframe after it's done with preprocessing to a new dataframe
df_afterPre = df_beforePre


########### --------  Feature Adaptation for better prediction  -------- ########### 
# Variables to adapt are driver_point, driver_wins, constructor_point, constructor_wins, fastestLap_time, q1_time, q2_time, q3_time, position

# Change position into the points winning positions and otherwise 0
df_afterPre['finishing_position'] = np.where(df_afterPre['position'] > 10, 0, (df_afterPre['position']))

# Points --> Changing the point variables to not include this race
df_afterPre['prev_driver_point'] = df_afterPre.driver_point - df_afterPre.points

df_afterPre3 = df_afterPre[['constructorId', 'raceId', 'points']]
df_afterPre3 = df_afterPre3.groupby(['constructorId', 'raceId',]).sum()
df_afterPre3 = df_afterPre3.rename(columns={"points": "tt_constructor_pt"})
df_afterPre = df_afterPre.merge(df_afterPre3,on=['raceId', 'constructorId'], how='left')
df_afterPre['prev_constructor_point'] = df_afterPre.constructor_point - df_afterPre.tt_constructor_pt
df_afterPre = df_afterPre.drop(['driver_point', 'constructor_point', 'tt_constructor_pt'], axis=1) 

# Wins --> Changing the win variables to not include this race
conditions = [df_afterPre.points >= 25, df_afterPre.points < 25]
values = [1,0]
df_afterPre['win'] = np.select(conditions, values)
df_afterPre['prev_driver_win'] = df_afterPre.driver_wins - df_afterPre.win

df_afterPre4 = df_afterPre[['constructorId', 'raceId', 'win']]
df_afterPre4 = df_afterPre4.groupby(['constructorId', 'raceId',]).sum()
df_afterPre4 = df_afterPre4.rename(columns={"win": "tt_constructor_win"})
df_afterPre = df_afterPre.merge(df_afterPre4,on=['raceId', 'constructorId'], how='left')
df_afterPre['prev_constructor_win'] = df_afterPre.constructor_wins - df_afterPre.tt_constructor_win
df_afterPre = df_afterPre.drop(['driver_wins', 'constructor_wins', 'win', 'tt_constructor_win'], axis=1)

# Positions --> Changing the position variables to not include this race for constructor and driver
df_afterPre5 = df_afterPre[['driverId', 'raceId', 'prev_driver_point']]
df_afterPre5["prev_driver_pos"] = df_afterPre5.groupby(['raceId'])["prev_driver_point"].rank("min", ascending=False)
df_afterPre5 = df_afterPre5.drop(['prev_driver_point'], axis=1)
df_afterPre = df_afterPre.merge(df_afterPre5,on=['raceId', 'driverId'], how='left')

df_afterPre6 = df_afterPre[['constructorId', 'raceId', 'prev_constructor_point']]
df_afterPre6 = df_afterPre6.groupby(['raceId', 'constructorId',]).sum()
df_afterPre6["prev_constructor_pos"] = df_afterPre6.groupby(['raceId'])["prev_constructor_point"].rank("min", ascending=False)
df_afterPre6 = df_afterPre6.drop(['prev_constructor_point'], axis=1)
df_afterPre = df_afterPre.merge(df_afterPre6,on=['raceId', 'constructorId'], how='left')

# Qualifying --> making the time variables into one hot encoded variations of if they were active in the qualification sessions or notM
df_afterPre['in_q1'] = np.where(df_afterPre['q1_time']!= 0, 1, 0)
df_afterPre['in_q2'] = np.where(df_afterPre['q2_time']!= 0, 1, 0)
df_afterPre['in_q3'] = np.where(df_afterPre['q3_time']!= 0, 1, 0)

# Qualifying --> Get the fastest time they did in qualifying, exluding 0, since this means they did not do a lap
df_afterPre.q1_time = df_afterPre.q1_time.replace(0, np.nan)
df_afterPre.q2_time = df_afterPre.q2_time.replace(0, np.nan)
df_afterPre.q3_time = df_afterPre.q3_time.replace(0, np.nan)

df_afterPre['BestQ_Lap'] = df_afterPre[['q1_time','q2_time','q3_time']].min(axis=1)
df_afterPre = df_afterPre.fillna({'BestQ_Lap':0,'q1_time':0,'q2_time':0,'q3_time':0,}) # NA means that there was no lap done, translating into a 0 value. 

# Qualifying --> Get the difference between best lap of the qualifying and the best lap of the driver that qualifying
df_afterPre1 = df_afterPre[df_afterPre['BestQ_Lap'] != 0]
df_afterPre1 = df_afterPre1[['BestQ_Lap', 'raceId']]
df_afterPre1 = df_afterPre1.groupby(['raceId']).min()
df_afterPre1 = df_afterPre1.rename(columns={"BestQ_Lap": "Overall_BestQ_Lap"})
df_afterPre = df_afterPre.merge(df_afterPre1,on='raceId', how='left')
df_afterPre['BestQ_Lap_Diff'] = df_afterPre.BestQ_Lap - df_afterPre.Overall_BestQ_Lap
df_afterPre = df_afterPre.drop(['Overall_BestQ_Lap'], axis=1)

# Start_time variable to a morning, midday or night variable
df_afterPre['morning_race'] = np.where(df_afterPre['start_time'] < 12, 1, 0)
df_afterPre['midday_race'] = np.where((df_afterPre['start_time'] > 12) & (df_afterPre['start_time'] < 18), 1, 0)
df_afterPre['evening_race'] = np.where(df_afterPre['start_time'] > 18, 1, 0)

# ########### --------  Get Dummies  -------- ########### --> Code from https://github.com/veronicanigro/Formula_1 with changes
df_dum = pd.get_dummies(df_afterPre, columns = ['circuitId', 'nationality', 'constructor_name'])

for col in df_dum.columns:
    if 'nationality' in col and df_dum[col].sum() < 70:
        df_dum.drop(col, axis = 1, inplace = True)
        
    elif 'constructor_name' in col and df_dum[col].sum() < 110:
        df_dum.drop(col, axis = 1, inplace = True)
        
    elif 'circuitId' in col and df_dum[col].sum() < 100:
        df_dum.drop(col, axis = 1, inplace = True)
    
    else:
        pass

df_with_dummies = df_dum

# If we dont use dummies
df_afterPre = df_afterPre.drop(['nationality', 'constructor_name'], axis=1)

# Make the dataframe for feature selection and prediction
df_afterPre = df_afterPre.drop(['position', 'points'], axis=1)
df_with_dummies = df_with_dummies.drop(['position', 'points'], axis=1)

df = df_afterPre

# Save the dataframe to output data folder
# path = 'PUT PATH HERE'
# df.to_csv(path+'df_without_dummies.csv')
# df_with_dummies.to_csv(path+'df_with_dummies.csv')
