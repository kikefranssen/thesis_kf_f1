import urllib.request
import urllib.error
import json
import requests
import pandas as pd

########### -------- Notes about the script -------- ###########
# The df_location dataset was made by combining the races.csv and circuits.csv data on the circuitId variable and selecting 
# the columns "'location', 'date', 'lat', 'lng', 'alt'", using the following code and code from the main data loading and 
# preprocessing script (selecting right years and combining dataframe)
## df_loc = df_years[['location', 'date', 'lat', 'lng', 'alt']]
## df_loc = df_loc.drop_duplicates()
## df_loc.to_csv("locations.csv")

########### -------- Citation-------- ###########
# Visual Crossing Corporation. (2022)). Visual Crossing Weather (2012-2021). [API]. Retrieved from https://www.visualcrossing.com/
# The company is Visual Crossing and the data service name is Visual Crossing Weather. The URL is https://www.visualcrossing.com/

########### -------- Code to load weather data -------- ###########
# Dataset with location information including latitude, longtitude and date
df_location = pd.read_csv(r'PUT PATH TO LOCATIONS.CSV FILE HERE')

# Loop through the dataframe to get the relevant information, put this into the API retrieval URl and writing it to the existing dataframe
for i, row in df_location.iterrows():
    lat = str(row['lat'])
    lon = str(row['lng'])
    time_stamp = str(row['date'])
    BaseUrl = "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/"
    EndUrl = "?unitGroup=metric&elements=datetime%2Ctempmax%2Ctempmin%2Ctemp%2Cdew%2Chumidity%2Cprecip%2Cprecipprob%2Cprecipcover%2Cpreciptype%2Cwindspeed%2Cwinddir%2Cvisibility&include=days&key={PUT YOUR KEY HERE}&contentType=json"
    url = BaseUrl + lat + "%2C" + lon + "/" + time_stamp + "/" + time_stamp + EndUrl
    response = requests.get(url)
    data = json.loads(response.text)
    data = data["days"]
    dict = {k:v for e in data for (k,v) in e.items()}
    for key in dict:
        df_location.loc[i,key] = dict[key]
    print(i)

########### -------- Print and Save Dataset -------- ###########
# print(df_location)
# df_location.to_csv("weather_data.csv")