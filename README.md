# Thesis - Formula 1 Race Prediction
Predict the four classes of finishing positions of the races in the Formula 1 season of 2021, based on racing data from 2012-2020. This project showcases the code used in my thesis for the Master Data Science and Society in the schoolyear 2021-2022. The code is purely to showcase the methods used in this process. 

## Project Description
Comparing different neural network architectures in the prediction of Formula 1 racing results. The data used in this project was collected via [Formula 1 World Championship (1950 - 2022)](https://www.kaggle.com/datasets/rohanrao/formula-1-world-championship-1950-2020), a Kaggle dataset compiled from the [Ergast API](http://ergast.com/mrd/terms/). Furthermore, the project also uses weather data from the Visual Crossings Weather, by the company Visual Crossing ( https://www.visualcrossing.com/).

## Table of contents
* [Description](#description)
* [Setup](#setup)

## Description
The code showcased in this project goes in numerical order. This section describes some necessary information about it. The code showcased in this project goes in numerical order. This section describes some necessary information about it. For the datasets in the code to be loaded, the file paths must be added. All the necessary files are in the data map. For the 1_load_weather.py file, the KEY is not added to the code for privacy reasons. The code used for graphs was not included. The numeric order of the files is to showcase in what order they were used for the project. 

## Setup
The following libraries are required to be installed: 
• Python
• Pandas
• Numpy
• Matplotlib
• Imblearn
• Tensorflow
• Keras
• JSON
• Requests
• Urllib
