########### -------- Libraries -------- ###########
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import f1_score
from imblearn.over_sampling import SMOTE
from sklearn.dummy import DummyClassifier
from sklearn.metrics import confusion_matrix
from sklearn.feature_selection import f_classif
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report
from sklearn.feature_selection import SelectKBest
from sklearn.model_selection import train_test_split

#Keras and Tensorflow Libraries
import tensorflow as tf
from tensorflow import keras
import tensorflow_addons as tfa

from keras import Sequential
from keras import backend as K
from keras.utils.vis_utils import plot_model
from keras.layers import Dense, Layer

########### -------- Loading in dataframes and dropping irrelevant columns -------- ###########
df = pd.read_csv(r'PUT PATH TO DF_WITHOUT_DUMMIES.CSV FILE HERE')
df_dum = pd.read_csv(r'PUT PATH TO DF_WITH_DUMMIES.CSV FILE HERE')
df_dum = df_dum.drop(['Unnamed: 0.2', 'Unnamed: 0.1', 'Unnamed: 0'], axis=1) 

########### -------- Preperation for Predicting and Feature Selection -------- ###########
# Turning the postions into 4 classes: Podium, Upper-midfield, Lower-midfield and no-points
conditions = [
    (df_dum['finishing_position'] == 0),
    (df_dum['finishing_position'] > 6),
    (df_dum['finishing_position'] > 3) & (df_dum['finishing_position'] < 7 ),
    (df_dum['finishing_position'] != 0) & (df_dum['finishing_position'] < 4),]
values = [0,1,2,3]
df_dum['fp'] = np.select(conditions, values)
df_dum = df_dum.drop(['finishing_position'], axis=1) 

# Train-Test split for Feature Selection
test = df_dum.loc[df['year'] == 2021]
train = df_dum.loc[df['year'] < 2021]
y_train = train['fp']
x_train =  train.drop(['fp'], axis=1)

#Select features based on best f1-score
selector = SelectKBest(f_classif, k=10)
selected_features = selector.fit_transform(x_train, y_train)
f_score_indexes = (-selector.scores_).argsort()[:18]

########### --------  The Train and Test Split  -------- ###########
test = df_dum.loc[df['year'] == 2021]
train = df_dum.loc[df['year'] < 2021]
y_train = train['fp']
x_train =  train.iloc[: , [ 9, 30, 29, 33, 24, 25, 26, 28, 27, 32, 82, 23,  3,  1, 77, 83, 78, 2]]
x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, 
test_size=0.25, random_state= 42)

y_test = test['fp']
x_test =  test.iloc[: , [ 9, 30, 29, 33, 24, 25, 26, 28, 27, 32, 82, 23,  3,  1, 77, 83, 78, 2]]

########### --------  Oversampling to treat imbalance & Scaling  -------- ###########
# The following code summarizes the distribution, this was used, but only to make graphs and look at the distribution
# from collections import Counter
# counter = Counter(y_train)
# for k,v in counter.items():
# 	per = v / len(y_train) * 100
# 	print('Class=%d, n=%d (%.3f%%)' % (k, v, per))
	
# # plot the distribution
# plt.bar(counter.keys(), counter.values(), color=('#67AB8E'))
# plt.show()

# Oversampling
strategy = {0:1500, 1:1500, 2:1500, 3:1500}
oversample = SMOTE(sampling_strategy=strategy)
x_train, y_train = oversample.fit_resample(x_train, y_train)

# Get dummies for the Y variables for training
y_train = pd.get_dummies(y_train)
y_test = pd.get_dummies(y_test)
y_val = pd.get_dummies(y_val)

#Scale
sc = StandardScaler()
x_train = sc.fit_transform(x_train)
x_test = sc.transform(x_test)
x_val = sc.transform(x_val)

########### --------  MODEL 1: Dummy Classifier  -------- ###########
# The classifier
dummy_clf = DummyClassifier(random_state = 42, strategy="most_frequent")
dummy_clf.fit(x_train, y_train)

# Evaluation Format
dum_y_test = np.array(y_test)
dum_y_test_arg=np.argmax(dum_y_test,axis=1)
dum_y_pred = np.argmax(dummy_clf.predict(x_test),axis=1)

#Classification Report
# print(classification_report(dum_y_test_arg, dum_y_pred))
#F1 score
dum_f1 = f1_score(dum_y_test_arg, dum_y_pred, average=None)
print("Per class F1 Score",  dum_f1, "and mean F1 Score", np.mean(dum_f1))

########### --------  MODEL 2: Deep Neural Network  -------- ###########
dnn_model = tf.keras.Sequential([
    tf.keras.layers.Dense(136, activation='tanh', input_shape=(18,)),
    tf.keras.layers.Dropout(.5),

    tf.keras.layers.Dense(136 , activation='tanh'),
    tf.keras.layers.Dropout(.5),
 
    tf.keras.layers.Dense(18, activation='tanh'),
    tf.keras.layers.Dropout(.5),
   
    tf.keras.layers.Dense(4, activation='softmax')])

# Compiler
dnn_model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
              loss=tf.keras.losses.CategoricalCrossentropy(),
              metrics=[tfa.metrics.F1Score(num_classes = 4, name='f1_score')])

# print(dnn_model.summary())

# Early Stopping Callback
es = keras.callbacks.EarlyStopping(monitor='val_loss', 
                                   mode='min',
                                   patience=50, 
                                   restore_best_weights=True)

# Training
dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_dataset = dataset.shuffle(len(x_train)).batch(50)
dataset = tf.data.Dataset.from_tensor_slices((x_val, y_val))
validation_dataset = dataset.shuffle(len(x_val)).batch(50)

history_dnn = dnn_model.fit(train_dataset, callbacks=[es], epochs=8000, validation_data=validation_dataset)

########### --------  Evaluation -------- ###########
dnn_test_loss, dnn_test_f1 = dnn_model.evaluate(x_test, y_test)
print("Per class F1 Score of",  dnn_test_f1, "and mean F1 Score of", np.mean(dnn_test_f1))

########### --------  MODEL 3: Radial Basis Function Network --- ###########
# The RBF layer: Code from https://www.kaggle.com/code/residentmario/radial-basis-networks-and-custom-keras-layers/notebook
class RBFLayer(Layer):
    def __init__(self, units, gamma, **kwargs):
        super(RBFLayer, self).__init__(**kwargs)
        self.units = units
        self.gamma = K.cast_to_floatx(gamma)

    def build(self, input_shape):
        self.mu = self.add_weight(name='mu',
                                  shape=(int(input_shape[1]), self.units),
                                  initializer='uniform',
                                  trainable=True)
        super(RBFLayer, self).build(input_shape)

    def call(self, inputs):
        diff = K.expand_dims(inputs) - self.mu
        l2 = K.sum(K.pow(diff, 2), axis=1)
        res = K.exp(-1 * self.gamma * l2)
        return res

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.units)

# The Radial Basis Function Network Model
rbf_model = tf.keras.Sequential([
    tf.keras.layers.InputLayer(18,),
    RBFLayer(512, 0.2),
    tf.keras.layers.Dense(4, activation='softmax')])
x_train = pd.DataFrame(x_train)

# Compiler
rbf_model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
              loss=tf.keras.losses.CategoricalCrossentropy(),
              metrics=[tfa.metrics.F1Score(num_classes = 4, name='f1_score')])

print(rbf_model.summary())

# Early Stopping Callback
es = keras.callbacks.EarlyStopping(monitor='val_loss', 
                                   mode='min',
                                   patience=50, 
                                   restore_best_weights=True)

# Training the RBF
dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_dataset = dataset.shuffle(len(x_train)).batch(32)
dataset = tf.data.Dataset.from_tensor_slices((x_val, y_val))
validation_dataset = dataset.shuffle(len(x_val)).batch(32)

history_rbf = rbf_model.fit(train_dataset, callbacks=[es], epochs=8000, validation_data=validation_dataset)

########### --------  Evaluation -------- ###########
rbf_test_loss, rbf_test_f1 = rbf_model.evaluate(x_test, y_test)
print("Per class F1 Score of",  rbf_test_f1, "and mean F1 Score of", np.mean(rbf_test_f1))