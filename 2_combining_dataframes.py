########### -------- Libraries -------- ###########
import pandas as pd

########### -------- Loading in the Dataframe & Selecting Features -------- ###########
#Loading in all the Kaggle Formula 1 Data. Some additions were manually mmade in the circuits data by adding an length of circuit and amount of turns variable
df_circuits = pd.read_csv(r'PUT PATH TO CIRCUITS.CSV FILE HERE')
df_constructor_stand = pd.read_csv(r'PUT PATH TO STANDING.CSV FILE HERE')
df_constructor = pd.read_csv(r'PUT PATH TO CONSTRUCTORS.CSV FILE HERE')
df_driver_standing = pd.read_csv(r'PUT PATH TO DRIVER_STANDINGS.CSV FILE HERE')
df_drivers = pd.read_csv(r'PUT PATH TO DRIVERS.CSV FILE HERE')
df_lap = pd.read_csv(r'PUT PATH TO LAP_TIMES.CSV FILE HERE')
df_pit = pd.read_csv(r'PUT PATH TO PIT_STOPS.CSV FILE HERE')
df_qualifying = pd.read_csv(r'PUT PATH TO QUALIFYING.CSV FILE HERE')
df_races = pd.read_csv(r'PUT PATH TO RACES.CSV FILE HERE')
df_results =  pd.read_csv(r'PUT PATH TO RESULTS.CSV FILE HERE')
df_status = pd.read_csv(r'PUT PATH TO STATUS.CSV FILE HERE')

#Loading in the API dataset, made in the load_weather.py file
df_weather = pd.read_csv(r'PUT PATH TO WEATHER_DATA.CSV FILE HERE')

########### -------- Selecting the relevant features and making them ready for preprocessing -------- ###########
# Getting the relevant columns from the dataframes that were chosen for prediction
races_col = df_races[['raceId', 'year', 'round', 'name', 'date', 'time', 'circuitId']]
drivers_col = df_drivers[['driverId', 'forename', 'surname', 'dob', 'nationality']]
circuit_col = df_circuits [['circuitId', 'alt','location', 'length', 'turns']]
constructor_col = df_constructor[['constructorId', 'name']]
quali_col = df_qualifying[['raceId', 'driverId','position', 'q2', 'q1', 'q3']]
d_stand_col = df_driver_standing[['raceId','driverId','points','position','wins']]
c_stand_col = df_constructor_stand[['raceId','constructorId','points','position','wins']]
weather_col = df_weather[['date','tempmax','tempmin','temp','dew','humidity','precip','precipcover','windspeed','winddir','visibility']]

# New dataframe with max amount of pitstops, first delete all other info in this dataset to be able to get the max() of stops per driveId&raceId combo
pit_col = df_pit.drop(columns=['lap', 'time', 'duration', 'milliseconds'])
pit_col = pit_col.groupby(['raceId','driverId']).max() 

# Make the base of the dataset, this is done with the results dataset since this dataset contains the variable that eventually needs to be predicted
df_combine = df_results[['resultId', 'raceId','driverId','constructorId','position','points','grid','laps','time','fastestLapTime','statusId']]

# Renaming the columns to avoid merging errors
races_col = races_col.rename(columns={'name': 'race_name', 'time': 'race_time'})
constructor_col = constructor_col.rename(columns={'name': 'constructor_name'})
quali_col = quali_col.rename(columns={'position': 'quali_position'})
d_stand_col = d_stand_col.rename(columns={'position': 'driver_pos', 'points': 'driver_point', 'wins': 'driver_wins'})
c_stand_col =  c_stand_col.rename(columns={'position': 'constructor_pos', 'points': 'constructor_point', 'wins': 'constructor_wins'})

# Merging the data into one dataset based on common values
df_combine = df_combine.merge(races_col,on='raceId',how='left')
df_combine = df_combine.merge(drivers_col,on='driverId',how='left')
df_combine = df_combine.merge(circuit_col,on='circuitId',how='left')
df_combine = df_combine.merge(constructor_col,on='constructorId',how='left')
df_combine = df_combine.merge(df_status,on='statusId',how='left')
df_combine = df_combine.merge(quali_col,on=['raceId', 'driverId'],how='left')
df_combine = df_combine.merge(d_stand_col,on=['raceId', 'driverId'],how='left')
df_combine = df_combine.merge(c_stand_col,on=['raceId', 'constructorId'],how='left')
df_combine = df_combine.merge(pit_col,on=['raceId', 'driverId'],how='left')
df_combine = df_combine.merge(weather_col,on=['date'],how='left')

# Filtering the dataset on years after and including 2012, which is the hybrid Formula 1 period
df_years = df_combine.loc[df_combine['year'] >= 2012]
df_beforePre = df_years[['raceId', 'driverId', 'constructorId', 'constructor_name','points',
       'position', 'laps', 'year', 'round','race_time', 'circuitId' ,'length', 'turns', 'alt','grid', 'q1', 'q2',
       'q3', 'driver_point','driver_wins', 'constructor_point','constructor_wins', 'tempmax', 'tempmin',
       'temp', 'dew', 'humidity', 'precip', 'precipcover', 'windspeed', 'winddir', 'visibility', 'dob', 'nationality']]

# path = 'PUT PATH HERE'
# df_beforePre.to_csv(path+'df_before_preprocessing.csv')